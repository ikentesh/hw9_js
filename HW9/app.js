"use strict";

/*Теоретичні питання

Опишіть, як можна створити новий HTML тег на сторінці.

Для того, щоб створити новий елемент із заданим тегом необхідно використовувати наступний метод: document.createElement("tag");
Після того, щоб наш tag з'явився на сторінці, нам потрібно вставити його десь у document. Наприклад, у document.body.
Для цього є метод append у нашому випадку: document.body.append(tag). Існують різні методи для різних варіантів вставки, append як приклад.

Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

У методі element.insertAdjacentHTML(where, html) перший параметр where - це спеціальне слово, що вказує, куди по відношенню до element робити вставку. 
Значення повинне бути одним із наступних:
"beforebegin" – вставити html безпосередньо перед elem,
"afterbegin" – вставити html на початок elem,
"beforeend" – вставити html в кінець elem,
"afterend" – вставити html безпосередньо після elem.


Як можна видалити елемент зі сторінки?

Для видалення елементу зі сторінки є методи node.remove().
Щоб видалити якийсь конкретний тег, запис буде наступним: tag.remove().*/

// Завдання

let arr = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
  ["1", "2", "sea", "user", 23, ["1", "2", "3", "sea", "user", 23]],
];

function createList(array, parent = document.body) {
  const ul = document.createElement("ul");
  parent.append(ul);

  array.forEach((el) => {
    if (Array.isArray(el)) {
      createList(el, ul);
    } else {
      let li = document.createElement("li");
      ul.append(li);
      li.innerHTML = el;
    }
  });
}

createList(arr);
